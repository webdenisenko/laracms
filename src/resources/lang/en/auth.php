<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'invalid' => 'Incorrect data. Please correct the errors and try again.',
    'failed' => 'Incorrect email or password',
    'throttle' => 'Too many login attempts. Please try again in :timeout minute(s).',

    'success' => 'You have successfully logged.'

];
