<?php
return [
    '403' => [
        'text'=>'Forbidden',
        'message'=>"Access is denied. You don't have access to view this page."
    ]
];