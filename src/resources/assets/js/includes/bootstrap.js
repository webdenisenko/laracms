export function $(selector){
    return selector.charAt(0) == '#' ? document.getElementById(selector.slice(1)) : document.querySelectorAll(selector);
}

export function hasClass(el, classNames){
    if(!el) return false;
    return el.classList ? el.classList.contains(classNames) : new RegExp('(^| )' + classNames + '( |$)', 'gi').test(el.className);
}

export function addClass(el, className){
    if(!el) return false;
    return el.classList ? el.classList.add(className) : el.className += ' ' + className;
}

export function removeClass(el, className){
    if(!el) return false;
    return el.classList ? el.classList.remove(className) : el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}

export function serializeArray(arr, name='') {
    let arrayStr = '';

    for (let key in arr) {
        if(arrayStr) arrayStr += '&';
        if(typeof arr[key] == 'object') arrayStr += serializeArray(arr[key], key);
        else arrayStr += (name||key) + (name?'['+key+']':'')+'=' + encodeURIComponent(arr[key]);
    }

    return arrayStr;
}

export function extend(out) {
    out = out || {};

    for (let i = 1; i < arguments.length; i++) {
        if (!arguments[i]) continue;

        for (let key in arguments[i]) {
            if (arguments[i].hasOwnProperty(key))
                out[key] = arguments[i][key];
        }
    }

    return out;
}

export function extendRecursive(out) {
    out = out || {};

    for (let i = 1; i < arguments.length; i++) {
        let obj = arguments[i];

        if (!obj) continue;

        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                typeof obj[key] === 'object' ? out[key] = extendRecursive(out[key], obj[key]) : out[key] = obj[key];
            }
        }
    }

    return out;
}