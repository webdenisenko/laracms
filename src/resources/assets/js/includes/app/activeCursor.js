import {$} from '../bootstrap';

let
  headerNavigate = $('#header-navigate'),
  activeLink = headerNavigate.querySelector('.active'),
  activeCursor = $('#active-cursor');

function setActiveCursor(elem) {
  activeCursor.style.width = elem.offsetWidth+'px';
  activeCursor.style.left = elem.offsetLeft+'px';
}

headerNavigate.onmouseover = headerNavigate.onmouseout = function(e){
  let elem = e.type == 'mouseover' ? e.target : activeLink;
  // If target is not a link
  if(elem.tagName != 'A'){
    elem = elem.parentNode;

    if(elem.tagName != 'A') return 0;
  }

  setActiveCursor(elem);
};

setActiveCursor(activeLink);