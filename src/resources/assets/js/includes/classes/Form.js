import {$, hasClass, addClass, removeClass, extend} from '../bootstrap';
import Ajax from './Ajax';

export default class {
  constructor(el){
    this.form = el;
  }

  reset(){
    [].forEach.call(this.form.querySelectorAll('input, textarea'), function(input){
      input.value = null;
    });
  }

  disabled(status){
    status ? addClass(this.form, 'disabled') : removeClass(this.form, 'disabled');
    [].forEach.call(this.form.querySelectorAll('input, textarea, button'), function(input){
      status ? input.setAttribute('disabled', 'disabled') : input.removeAttribute('disabled');
    });
  }

  ajaxSubmit(config){
    let
      _this = this,
      settings = {
        systemErrors: null,
        events: {
          success: function(){},
          invalid: function(res){
            for (let name in res.response.data) {
              if(name == '_system'){
                if(settings.systemErrors){
                  removeClass(settings.systemErrors, 'hide');
                  settings.systemErrors.innerHTML = res.response.data._system;
                }
                continue;
              }

              let input = _this.form.querySelector('[name="'+name+'"]');

              if(input){
                addClass(input, 'input-error');

                if(!$(':focus').length){
                  input.select();
                }

                // Create text element if it not exist
                if(!hasClass(input.nextElementSibling, 'status-text')) input.insertAdjacentHTML('afterend', '<div class="status-text"/>');
                input.nextElementSibling.innerText = res.response.data[name][0];
              }
            }
          },
          error: function(){},
        }
      };

    if(config.systemErrors) settings.systemErrors = config.systemErrors;
    settings.events = extend(settings.events, config.events);

    // Disable default HTML form validate
    _this.form.setAttribute('novalidate', 'novalidate');

    // Submit event
    _this.form.addEventListener('submit', function (e) {
      // Disable default form action
      e.preventDefault();

      if(hasClass($('body'), 'ajax')) return false;

      // Drop focus from submit btn
      $(':focus')[0].blur();

      // Get and Validate form data
      let
        formData = {},
        allow = true;

      [].forEach.call(e.target.querySelectorAll('input[name], textarea[name]'), function(input){
        // Save input data
        formData[input.getAttribute('name')] = input.value;

        // Check
        if(!input.value && input.getAttribute('type') != 'hidden'){
          // Disallow ajax submit
          allow = false;

          // Show error
          addClass(input, 'input-error');
          if(!$(':focus').length){
            input.select();
          }
        }else{
          removeClass(input, 'input-error');

          // Remove caption under input if it exists
          if(hasClass(input.nextElementSibling, 'status-text')) delete input.nextElementSibling.remove();
        }
      });

      // Send ajax query if no errors
      if(allow){
        // Disable form
        _this.disabled(true);

        // Hide header container for system messages
        addClass(settings.systemErrors, 'hide');

        let request = new Ajax({
          type: _this.form.getAttribute('method'),
          url: _this.form.getAttribute('action'),
          data: formData,
          dataType: 'json',
          complete: function(res){
            // Enable form
            _this.disabled(false);

            if(res.statusText == 'OK'){
              if(typeof res.response == 'object'){
                if(settings.events[res.response.status]) return settings.events[res.response.status](res);
              }else console.error('Server error:\n\n'+res.response);
            }else console.error('Query error #'+res.status);
          }
        });
      }
    });
  }
}