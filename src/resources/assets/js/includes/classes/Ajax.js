import {$, addClass, removeClass, serializeArray} from '../bootstrap';

export default class {
  constructor(config) {
    this.request = new XMLHttpRequest();
    this.request.open( (config.type = (config.type || 'GET').toUpperCase()), config.url || '', !config.async);

    if (config.dataType) this.request.responseType = config.dataType;

    this.request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    let CSRFtoken;
    if (config.data && config.data.hasOwnProperty('_token')) {
      CSRFtoken = config.data._token;
      delete config.data._token;
    }else CSRFtoken = $('meta[name="csrf-token"]')[0].getAttribute('content');

    if(config.type == 'POST'){
      this.request.setRequestHeader('X-CSRF-TOKEN', CSRFtoken);
    }

    if (config.headers) {
      for (let key in config.headers) {
        this.request.setRequestHeader(key, config.headers[key]);
      }
    }

    let $body = $('body');

    if (config.complete) this.request.onload = function () {
      removeClass($body, 'ajax');
      config.complete(this);
    };

    if (config.error) this.request.onerror = function () {
      removeClass($body, 'ajax');
      config.error(this);
    };

    let data = config.data || null;
    if (typeof config.data == 'object') data = serializeArray(data);

    addClass($body, 'ajax');
    return this.request.send(data);
  }
}