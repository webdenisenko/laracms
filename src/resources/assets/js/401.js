import {$, addClass, removeClass} from './includes/bootstrap';
import Form from './includes/classes/Form';

let
  LoginForm = new Form($('#login-form')),
  sysMessContainer = $('#system-message');

LoginForm.ajaxSubmit({
  systemErrors: sysMessContainer,
  events: {
    success: function (res){
      removeClass(sysMessContainer, 'danger');
      addClass(sysMessContainer, 'success');
      removeClass(sysMessContainer, 'hide');
      sysMessContainer.innerHTML = res.response.data;
      location.reload();
    }
  }
});