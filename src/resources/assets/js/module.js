import {$, removeClass} from './includes/bootstrap'
import VanillaModal from 'vanilla-modal';
import Ajax from './includes/classes/Ajax';
import Form from './includes/classes/Form';

const modal = new VanillaModal();

let modalForm = $('#modal-module-form');

let request = new Ajax({
  type: 'POST',
  url: modalForm.getAttribute('data-url'),
  dataType: 'text',
  complete: function(res){
    // Remove preloader
    modalForm.innerHTML = '';

    // Include form
    if(res.statusText == 'OK'){
      modalForm.innerHTML = res.responseText;
      // TODO set ajax for form
      let form = new Form($('#data-form'));
    }
  }
});