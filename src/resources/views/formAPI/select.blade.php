<div class="input-set">
    <select
            class="input input-block"
            type="text"
            name="{{ $field->name }}-module-data"
            {{ $field->required ? 'required' : '' }}
    >
        <option value="-1" class="placeholder">- {{ \Illuminate\Support\Facades\Lang::has("fields." . $form->name . ".$field->name") ? trans("fields." . $form->name . ".$field->name") : "Select $field->name" }} -</option>
        @foreach($field->options as $k=>$option)
            <option value="{{ $k }}">{{ \Illuminate\Support\Facades\Lang::has("fields." . $form->name . ".$option") ? trans("fields." . $form->name . ".$option") : title_case($option) }}</option>
        @endforeach
    </select>
</div>