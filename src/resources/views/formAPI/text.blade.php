<div class="input-set">
    <input
            class="input input-block"
            type="text"
            name="{{ $field->name }}-module-data"
            placeholder="{{ \Illuminate\Support\Facades\Lang::has("fields." . $form->name . ".$field->name") ? trans("fields." . $form->name . ".$field->name") : title_case($field->name) }}"
            {{ $field->required ? 'required' : '' }}
    >
</div>