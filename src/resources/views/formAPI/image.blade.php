<div class="input-set">
    <input
            class="input input-block"
            type="file"
            name="{{ $field->name }}-module-data"
            {{ $field->required ? 'required' : '' }}
    >
</div>