@extends('laracms::index')

@section('page')
    <aside id="sidebar">
        @foreach($navigate as $category=>$itemsList)
            <nav class="nav-box">
                <h3 class="box-title">{{ $category }}</h3>

                <ul class="links">
                    @foreach($itemsList as $name=>$slug)
                        <li><a href="{{ route('admin.module', ['module'=>$slug]) }}">{{ $name }}</a></li>
                    @endforeach
                </ul>
            </nav>
        @endforeach

        <nav class="nav-box">
            <h3 class="box-title">System</h3>

            <ul class="links">
                <li><a class="active" href="#">File manager</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="#">Users</a></li>
            </ul>
        </nav>
    </aside>

    <section id="content">@yield('content')</section>

    <div class="modal">
        <div class="modal-inner">
            <div class="modal-header">
                <div class="modal-title"></div>
                <a data-modal-close>{!! svg('close') !!}</a>
            </div>
            <div class="modal-content"></div>
        </div>
    </div>
@endsection