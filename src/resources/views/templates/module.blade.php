@extends('laracms::templates.main')

@section('head')
    {{ style('module') }}
@endsection

@section('content')
    <div class="head-panel">
        <h1 class="module-name">{{ $model->getName() }}</h1>

        <form class="input-set input-set-inline search-form">
            <div class="icon">{!! svg('search') !!}</div>
            <input type="text" class="input search" id="search-in-model" placeholder="Search...">
        </form>
        shown
        <select class="input num" id="amt">
            <option value="1">10</option>
            <option value="2">25</option>
            <option value="3">50</option>
            <option value="3">100</option>
        </select>
        of <span class="amount" id="amt">133</span>

        <a href="#modal-module-form" data-modal-open class="btn btn-add" id="add-item"><span class="svg-icon">{!! svg('plus') !!}</span></a>
    </div>

    <table class="module-data table" id="module-data">

        <thead class="head">
            <tr>
                <td class="col-name col-id"><input type="checkbox" class="checkbox" id="select-all-items"></td>
                <td class="col-name">Name</td>
                <td class="col-name">Position</td>
                <td class="col-name">Lust update</td>
                <td class="col-name" width="70"></td>
            </tr>
        </thead>

        <tbody>
            <tr class="item">
                <td class="col-name col-id"><input type="checkbox" class="checkbox"></td>
                <td class="col-name">Roman Denisenko</td>
                <td class="col-name">Owner</td>
                <td class="col-name col-date">29.07.97 at 16:53</td>
                <td class="col-name"><a href="#edit" class="btn btn-gray"><span class="svg-icon">{!! svg('edit') !!}</span></a></td>
            </tr>
        </tbody>

    </table>

    <div class="pagination" id="pagination">
        <a href="#">1</a>
        <a class="active" href="#">2</a>
        <a class="next" href="#">3 next</a>
        <a href="#">4</a>
        <a href="#">5</a>
        <a href="#">...128</a>
    </div>

    <div class="hide" id="modal-module-form" data-url="{{ route('admin.add', ['module'=>$model->getName()]) }}"></div>
@endsection

@section('js')
    {{ script('module') }}
@endsection