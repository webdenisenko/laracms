<form id="data-form" action="{{ route('formAPI.action', ['form' => $form->name]) }}" method="post">
    @foreach ($form->fields as $field)
        @if(file_exists(package_path("resources/views/formAPI/$field->type.blade.php")))
            @include('laracms::formAPI.' . $field->type, ['field' => $field, 'form' => $form])
        @else
            @include('laracms::formAPI.text', ['field' => $field, 'form' => $form])
        @endif
    @endforeach

    {{ csrf_field() }}

    <button class="btn float-right" type="submit" tabindex="3">Done <span class="svg-icon svg-icon-right">{!! svg('success') !!}</span></button>
    <div class="clearfix"></div>
</form>