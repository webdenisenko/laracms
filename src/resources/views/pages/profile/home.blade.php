@extends('layouts.admin')

@section('title', $profile->name)

@section('head')
    <link rel="stylesheet" href="/public/css/admin/profile.css">
@endsection

@section('content')
    <form action="/" class="profile-form">
        <h1>Profile data:</h1>

        <div class="row">
            <div class="col-xs-4">
                <img src="{{ $profile->photo ?: 'admin.default_user_photo' }}" alt="{{ $profile->name }}">
            </div>
            <div class="col-xs-8">
                asd
            </div>
        </div>
    </form>
@endsection