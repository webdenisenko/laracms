<div class="logo">
    <a href="{{ route('admin.home') }}"><img src="{{ imgurl('logo-header.png') }}" alt="{{ config('app.name') }}"></a>
</div>

<div class="nav">
    <div id="active-cursor" class="active-cursor"></div>
    <ul id="header-navigate">
        <li><a class="active" href="{{ route('admin.home') }}">Management</a></li>
        <li><a href="{{ route('admin.profile', ['user'=>$usr->user_id]) }}">Notifications <span class="num">11</span></a></li>
        <li><a href="{{ '#' }}">Settings</a></li>
    </ul>
</div>

<div class="profile">
    <div class="photo"><img src="{{ $usr->photo ?: imgurl('default-user.jpg') }}" alt="{{ $usr->name }}"></div>
    <div class="menu">
        <ul>
            <li><a href="{{ route('admin.profile', ['user'=>$usr->user_id]) }}">Profile</a></li>
            <li><a href="#">Edit</a></li>
            <li><a href="{{ route('out') }}">Log-out</a></li>
        </ul>
    </div>
</div>