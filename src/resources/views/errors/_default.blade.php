<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error @yield('code', $code?:'') - @yield('text', $text?:'undefined error')</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" href="/css/errors/error.css">
</head>
<body>
<div id="wrap">
    <div class="box">
        <div class="error-code">Error <span class="code">#@yield('code', $code?:'000')</span></div>
        <div class="error-text">@yield('text', $text?:'undefined error')</div>
        <div class="error-message"><b>@yield('message', $message?:'')</b></div>
        <div class="solutions">
            <a href="#" onclick="history.back()" class="link">Back</a>
            <a href="/" class="btn home">Home</a>
        </div>
    </div>
</div>
</body>
</html>