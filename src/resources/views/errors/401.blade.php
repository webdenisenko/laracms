<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} - Authentication</title>
    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--Style--}}
    {{ style('errors/401') }}

    @yield('head')
</head>
<body>
<section id="content">
    <form id="login-form" action="{{ route('auth') }}" method="post">
        <img class="form-title img-center" src="{{ imgurl('logo-welcome.png') }}" alt="welcome-logo">

        <div id="system-message" class="system-message notification danger hide"></div>

        <div class="input-set">
            <input class="input input-block" type="email" id="login-input" name="email" placeholder="E-Mail" tabindex="1" autofocus required>
        </div>

        <div class="input-set">
            <input class="input input-block" type="password" name="password" id="password-input" placeholder="Password" tabindex="2" required>
        </div>

        <a href="/" class="remind-password-link float-left">Forgot password?</a>

        {{ csrf_field() }}

        <button class="btn float-right" type="submit" tabindex="3">LOGIN <span class="svg-icon svg-icon-right">{!! svg('login') !!}</span></button>
        <div class="clearfix"></div>
    </form>
</section>

{{--JS--}}
{{ script('401') }}
</body>
</html>