<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', config('app.name')) - Control panel</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

    {{--Style--}}
    {{ style('app') }}

    @yield('head')
</head>
<body>
<header id="header">@include('laracms::header')</header>
<div id="wrap">
    @yield('page')
    <footer id="footer">@include('laracms::footer')</footer>
</div>
{{--JS--}}
{{ script('app') }}
@yield('js')
</body>
</html>