<?php
namespace Webdenisenko\Laracms;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Webdenisenko\Laracms\Commands\CreateAdminModel;
use Webdenisenko\Laracms\Commands\CreateDatabase;

class LaracmsServiceProvider extends ServiceProvider {

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(Router $router) {

        // use this if your package has views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'laracms');

        // use this if your package has lang files
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'laracms');

        // use this if your package has routes
        $router->group(['namespace' => 'Webdenisenko\Laracms\Controllers', 'middleware' => 'web'], function() {
            require __DIR__.'/routes.php';
        });

        // load our migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        // publish config if necessary
        $this->publishes([
            // TODO publish config files
            // __DIR__ . '/config/' => config_path(),
            __DIR__ . '/public/' => public_path(config('laracms.resources_path')),
            __DIR__ . '/AdminModels/Presintation' => app_path(config('laracms.models_dir'))
        ], 'laracms');

        // middleware
        $router->middleware('admin', 'Webdenisenko\Laracms\Middleware\Admin');

        // console commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateAdminModel::class,
                CreateDatabase::class,
            ]);
        }

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register() {

        $this->app->register('Webdenisenko\Laracms\Providers\AdminServiceProvider');
        $this->app->register('Webdenisenko\Laracms\Providers\ExtendAppServiceProvider');

        // use the default configuration file as fallback
        $this->mergeConfigFrom(__DIR__.'/config/laracms.php', 'laracms');

    }

}