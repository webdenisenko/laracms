<?php

namespace Webdenisenko\Laracms\Providers;

use App\Console\Commands\Admin;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Webdenisenko\Laracms\Controllers\AdminModelController;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('laracms::*', function ($view) {

            // Generate navigate from models folder
            $navigate = [];

            foreach (AdminModelController::getAllModules() as $model) {

                // Create category in array (if not exist)
                if (!isset($navigate[$model->getCategory()])) $navigate[$model->getCategory()] = [];

                // Save navigate-item in array
                $navigate[$model->getCategory()][$model->getName()] = snake_case(str_replace(config('laracms.model_prefix'), '', $model->getName()));

            }

            $view->with([
                'navigate' => $navigate
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
