<?php

namespace Webdenisenko\Laracms\Providers;

use App\Providers\AppServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class ExtendAppServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Custom help functions
        include(__DIR__.'../../helpers.php');

        // Default app variables
        View::composer('*', function ($view) {
            $view->with([
                'usr' => Auth::user()
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
