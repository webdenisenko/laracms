<?php

// Auth
Route::post('auth', 'AuthController@authenticate')->name('auth');
Route::get('out', 'AuthController@logout')->name('out');

// Admin
Route::group(['prefix' => config('laracms.url'), 'middleware' => 'admin'], function () {

  // General
  Route::get('/', 'AdminController@home')->name('admin.home');

  // Profile
  Route::get('/profile/{user?}', 'AdminController@profile')->name('admin.profile');

  // Module data
  Route::get('/module/{module}', 'AdminController@module')->name('admin.module');

  // Add item in module
  // TODO change `any` to `post`
  Route::any('/module/{module}/add', 'AdminController@add')->name('admin.add');

});

// FormAPI intermediary
Route::post('/form/{form}', 'FormAPIController@intermediary')->name('formAPI.action');