<?php

namespace Webdenisenko\Laracms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Webdenisenko\Laracms\Controllers\AdminModelController;

class CreateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:db {model?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create database table from Admin Models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        AdminModelController::createDB($this->argument('model'));
        $this->info("Status: success");
    }
}
