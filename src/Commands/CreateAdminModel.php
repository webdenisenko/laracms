<?php

namespace Webdenisenko\Laracms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class CreateAdminModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:model {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin model. php artisan admin:model "ModelName"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $modelName = $this->argument('model').config('laracms.model_prefix');
        $modelPath = app_path(config('laracms.models_dir')."/$modelName.php");

        if(file_exists($modelPath)) return $this->error("`$modelName` already exist!");

        try {
            $newModel = fopen($modelPath, "w");
            fwrite($newModel, str_replace('%module%', $modelName, file_get_contents(__DIR__ . '/../AdminModels/model.stub')));
            fclose($newModel);
        }catch (\Exception $e){
            $this->error($e->getMessage());
            return $this->error("`$modelName` has not been created!");
        }

        Artisan::call('optimize');

        $this->info("`$modelName` successfully created!");
    }
}
