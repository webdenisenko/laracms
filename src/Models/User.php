<?php

namespace Webdenisenko\Laracms\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static public function user_id(){
        return Auth::user()->user_id;
    }

    static public function group(){
        return Auth::user()->group;
    }

    static public function access($module){
        return UsersAccess::where(function ($q){
            $q->where('owner', User::user_id())->orWhere('owner', User::group());
        })->where('module', $module)->value('access_level');
    }
}
