<?php

namespace Webdenisenko\Laracms\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Exception;

class FormAPIController extends Controller{

  static function generateHTML($form){
    foreach ($form->fields as $name=>&$field){
      $field = FormAPIController::field($field)->getColumnt($field);
      $field->name = $name;
    }

    return view('laracms::templates.form', ['form'=> (object) $form]);
  }

  static function createDB($form){
    try{

      $tableName = "lcam_" . $form->name;

      if(Schema::hasTable($tableName)) throw new Exception('exist');

      return Schema::create($tableName, function (Blueprint $table) {
        $table->increments('id');

        foreach ($form->fields as $name=>$field){
          $field->name = $name;
          FormAPIController::field($field)->setSchema($table, $field);
        }

        $table->timestamps();
      });

    }catch (Exception $e){
      dd($e->getMessage());

      switch ($e->getMessage()){
        default: return $e;
      }
    }
  }

  static function intermediary($form, Request $request){
    $formData = $request->all();
dd($form);
    foreach ($form->fields as $name=>$field){
      FormAPIController::field($field)->format($field, $formData[$name]);
    }
    dd($formData);

    return $formData;
  }

  static function field($field){
    return app('Webdenisenko\\Laracms\\Controllers\\FieldsDB\\' . studly_case($field->type) . 'FieldController');
  }

}
