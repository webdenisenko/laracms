<?php

namespace Webdenisenko\Laracms\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Webdenisenko\Laracms\Controllers\AdminModelController;
use Webdenisenko\Laracms\Controllers\FormAPIController;

class AdminController extends Controller
{

  // Dashboard
  public function home(Request $request){
    return view('laracms::templates.home');
  }

  // View module data
  public function module($moduleSlug){
    $model = AdminModelController::getModule($moduleSlug);
    return view('laracms::templates.module', ['module'=>$moduleSlug, 'model'=>$model]);
  }

  // Add module item
  public function add($moduleSlug){
    $module = AdminModelController::getModule($moduleSlug);

    return FormAPIController::generateHTML( (object) [
      'name' => $module->getName(),
      'fields' => $module->getFields(),
      // TODO Set action url for modules form
      'action' => 'url/to/action'
    ] );
  }

  // User profile
  public function profile($userId=null){
    // Get current user If $userId not exists or Get $userId user or return 404 error
    if( ! $profile = $userId == User::user_id() || !$userId ? Auth::user() : $profile = User::where('user_id', $userId)->first()  ) return abort(404);

    return view('laracms::templates.profile.home', [
      'profile' => $profile
    ]);
  }

}
