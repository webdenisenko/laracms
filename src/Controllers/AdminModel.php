<?php

namespace Webdenisenko\Laracms\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminModel extends Controller
{
    // Default name
    protected $name = 'Nameless module';

    // Default category
    protected $category = 'Content';

    // Fields
    protected $fields = [];

    // Rules for validation
    protected $rules = [];


    public function getName(){
        return $this->name;
    }

    public function getCategory(){
        return $this->category;
    }

    public function getFields(){
        $fields = $this->fields;

        foreach ($fields as $name=>&$field){
            $field['name'] = $name;
            $field = (object) $field;
        }

        return (object) $fields;
    }


    // Event `View`
    public function View($data){
        return $data;
    }

    // Event `View for form(add/edit)`
    public function FormView($data){
        return $data;
    }

    // Event `Before addition`
    public function Addition($data){
        return $data;
    }

    // Event `Before editing`
    public function Editing($data){
        return $data;
    }

    // Event `Before Trash`
    public function Trash($index){}

    // Event `Before Destroy`
    public function Destroy($index){}

    // Event `Before Recovery`
    public function Recovery($index){}

}
