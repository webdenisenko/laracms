<?php

namespace Webdenisenko\Laracms\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Schema\Blueprint;

class FieldsDB extends Controller
{

  // Database schema
  protected $schema;

  // Filed parameters
  protected $field;

  // Required parameters for all fileds
  protected static $required = [
    'name'     => null,
    'type'     => null,
    'required' => null,
    'default'  => null
  ];

  // Default parameters for curr filed (editing from child class)
  protected $primary = [];

  // Bootstrap function
  public function setSchema(Blueprint $schema, $field) {
    $this->schema = $schema;
    $this->field = $field;

    $this->field = $this->getColumnt($field);

    $this->columnType();

    return $this->schema;
  }

  protected function getColumnt($field){
    $field = replaceNullItems((array) $field, $this::$required);
    $field = replaceNullItems($field, $this->primary);

    return (object) $field;
  }

  protected function format($field, $data){
    return $data;
  }

  // Set database columnt type
  protected function columnType(){
    $this->schema->string($field->name);
    if( ! $this->field->required) $this->schema->nullable();
  }

}
