<?php

namespace Webdenisenko\Laracms\Controllers;

use App\Http\Controllers\Controller;
use Webdenisenko\Laracms\Controllers\AdminController;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Webdenisenko\Laracms\Controllers\FormAPIController;
use Exception;

class AdminModelController extends Controller
{

  static function getAllModules(){
    $modules = [];

    foreach (scandir(app_path(config('laracms.models_dir')), 1) as $modelName) {

      // Skip not admin models
      if (!strpos($modelName, config('laracms.model_prefix'))) continue;

      // Module name without extension
      $modelName = basename($modelName, '.php');

      /**
       * Get model class
       *
       * @var App\Http\Controllers\AdminModel $model
       */
      array_push($modules, app("App\\AdminModels\\$modelName"));
    }

    return $modules;
  }

  static function getModule($moduleSlug){
    return app("App\\AdminModels\\" . studly_case($moduleSlug) . config('laracms.model_prefix'));
  }

  static function createDB($modelName=null){

    $create = function ($module){
      return FormAPIController::createDB([
        'name' => $module->getName(),
        'fields' => $module->getFields()
      ]);
    };

    // Create DB for Once model
    if($modelName) $create(AdminModelController::getModule($modelName));

    // Create DB for all models
    else foreach (AdminModelController::getAllModules() as $model) $create($model);

  }

}
