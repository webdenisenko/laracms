<?php

namespace Webdenisenko\Laracms\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    public function authenticate(Request $request){
        try{
            // Security
            if($request->url() != route('auth')) throw new Exception('incorrectRefPath');
            if($request->method() != 'POST') throw new Exception('incorrectMethod');

            // Throttling
            $timeout = session('auth_timeout') ? config('laracms.timeout') - (time() - session('auth_timeout', time())) : 0;
            if($timeout > 0) throw new Exception('throttling');

            $attempts = session('auth_attempts', 0);
            if($attempts > config('laracms.max_attemts')) throw new Exception('timeout');
            session(['auth_attempts'=>$attempts+1]);

            // Validation
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|between:8,64',
            ]);

            if($validator->fails()) throw new Exception('invalid');

            // User check
            if(!Auth::attempt(['email' => $request->input('email'), 'password' => $request->password], false)){
                throw new Exception('userNotFound');
            }

            session(['auth_attempts' => 0]);

            return resJSON('success', trans('laracms::auth.success'));
        }catch (Exception $e){
            $data = [
                '_system' => trans('laracms::standard.undefined')
            ];

            switch ($e->getMessage()){
                // Incorrect referral url or method is not ajax
                case 'incorrectRefPath':case 'incorrectMethod': return abort(423);

                // Throttling (is)
                case 'throttling':
                    $data['_system'] = trans('laracms::auth.throttle', ['timeout'=> ceil($timeout/60) ]);
                    break;

                // Throttling (start)
                case 'timeout':
                    session(['auth_attempts' => 0]);
                    session(['auth_timeout' => time()]);
                    $data['_system'] = trans('laracms::auth.throttle', ['timeout'=> ceil($timeout/60) ]);
                    break;

                // Invalid Login/Password
                case 'invalid':
                    $data['_system'] = trans('laracms::auth.invalid');
                    $data = array_merge($data, $validator->errors()->toArray());
                    break;

                // Current user not found
                case 'userNotFound':
                    $data['_system'] = trans('laracms::auth.failed');
                    break;
            }
            return resJSON('invalid', $data);
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->back();
    }
}
