<?php

namespace Webdenisenko\Laracms\Controllers\FieldsDB;

use App\Http\Controllers\Controller;
use Webdenisenko\Laracms\Controllers\FieldsDB;

class ImageFieldController extends FieldsDB
{

  protected $primary = [
    'max' => 1
  ];

  function ColumnType(){
    $this->schema->text($this->field->name);
  }

}
