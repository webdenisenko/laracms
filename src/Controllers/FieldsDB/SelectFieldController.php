<?php

namespace Webdenisenko\Laracms\Controllers\FieldsDB;

use App\Http\Controllers\Controller;
use Webdenisenko\Laracms\Controllers\FieldsDB;

class SelectFieldController extends FieldsDB
{

  protected $primary = [
    'max' => 1,
    'options' => [
      'empty'
    ]
  ];

  function ColumnType(){
    $this->schema->enum($this->field->name, $this->field->options);
  }

}
