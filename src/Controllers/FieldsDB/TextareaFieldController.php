<?php

namespace Webdenisenko\Laracms\Controllers\FieldsDB;

use App\Http\Controllers\Controller;
use Webdenisenko\Laracms\Controllers\FieldsDB;

class TextareaFieldController extends FieldsDB
{

  protected $primary = [
    'max' => 65535
  ];

  function ColumnType(){
    if($this->field->max > 16777215) return $this->schema->longText($this->field->name);

    if($this->field->max > 65535) return $this->schema->mediumText($this->field->name);

    if($this->field->max > 255) return $this->schema->text($this->field->name);

    return $this->schema->string($this->field->name, $this->field->max);
  }

}
