<?php
return [
    'url'            => 'cp',
    'model_prefix'         => 'AdminModel',
    'resources_path' => 'laracms',
    'models_dir'     => 'AdminModels',

    // Authentication
    'max_attemts'    => 6,
    'timeout'        => 300
];