<?php

function script(){
  echo '<script>';
  foreach (func_get_args() as $path){
    @include(public_path(config('laracms.resources_path') . "/js/$path.js"));
  }
  echo '</script>';
}

function style(){
  foreach (func_get_args() as $path){
    echo '<link rel="stylesheet" href="/'. config('laracms.resources_path') . "/css/$path" .'.css">';
  }
}

function svg($path){
  return @file_get_contents(public_path(config('laracms.resources_path') . "/img/$path.svg"));
}

function imgurl($path){
  return asset(config('laracms.resources_path') . "/img/$path");
}

function replaceNullItems($arr1, $arr2){
  foreach ($arr2 as $key=>$val){
    if(!isset( $arr1[$key] )) $arr1[$key] = $val;
  }
  return $arr1;
}

function package_path($path){
  return __DIR__ . '/' . $path;
}

function resJSON($status, $data=null){
  return [
    'status' => $status,
    'data' => $data
  ];
}