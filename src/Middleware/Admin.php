<?php

namespace Webdenisenko\Laracms\Middleware;

use Webdenisenko\Laracms\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Unauthorized
        if( ! Auth::check()){
            return abort(401);
        }

        // Forbidden
        if( ! User::access('admin')){
            return abort(403);
        }

        return $next($request);
    }
}
