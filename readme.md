## Initialization

1. Add a service provider in **config/app.php**:
`Webdenisenko\Laracms\LaracmsServiceProvider::class`

2. Replace **Exceptions/Handler.php** in your laravel project to Exceptions/Handler.php from package

3. Run a command:
`php artisan vendor:publish --provider="Webdenisenko\Laracms\LaracmsServiceProvider" --force`

4. Run a command:
`php artisan migrate`

## Commands

Create new Admin Model: `php artisan admin:model "ModelName"`

Create Database from Admin Models: `php artisan admin:db "(optional)ModelName"`